(($, window) => {
    $(window.document).ready(() => {
        $("#shortenBtn").on('click', () => {(new Shortener("#link", "#outputArea")).shorten() });
    });
})(jQuery, window);

Urls = {
    postLink: "https://kfugkmg2ui.execute-api.us-east-2.amazonaws.com/Dev/links",
    redirect: "https://kfugkmg2ui.execute-api.us-east-2.amazonaws.com/Dev/l/"
};

class Shortener {
    constructor(linkSourceId, outputAreaId) {
        this.linkSourceId = linkSourceId;
        this.outputAreaId = outputAreaId;
    }

    shorten() {
        $.ajax({
            method: "POST",
            dataType: "json",
            contentType: "application/json",
            url: Urls.postLink,
            data: JSON.stringify({ link: $(this.linkSourceId).val() })
        })
        .fail((error) => $(this.outputAreaId).text("Oops, something went wrong!"))
        .done((response) => {
            let link = Urls.redirect + response.CompressedLink;
            $(this.outputAreaId).html('<a  href="' + link + '" target="_blank">lightlink.com/l/' + response.CompressedLink + '</a>');
        });
    }
}
